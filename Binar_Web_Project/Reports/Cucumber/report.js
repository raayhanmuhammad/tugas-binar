$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Register.feature");
formatter.feature({
  "name": "Register",
  "description": "As a user, i want to register in Secondhand Store web.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Register"
    }
  ]
});
formatter.scenario({
  "name": "REG001 - User want to register without input nama, email and password",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG001"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \u0027Please fill out this field.\u0027 at Nama field",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.error_message_will_be_shown_at_Nama_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG002 - User want to register without input nama",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG002"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Email \u0027testadmin@gmail.com\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_Email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027xxxxxx\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \u0027Please fill out this field.\u0027 at Nama field",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.error_message_will_be_shown_at_Nama_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG003 - User want to register without input email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG003"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_Nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027xxxxxx\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \u0027Please fill out this field.\u0027 at Email field",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.error_message_will_be_shown_at_Email_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG004 - User want to register without input password",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG004"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_Nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email \u0027testadmin@gmail.com\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \u0027Please fill out this field.\u0027 at Password field",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.error_message_will_be_shown_at_Password_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG005 - User want to register without using format email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG005"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_Nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email without format \u0027testadmin\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_input_email_without_format(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User password \u0027xxxxxx\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \"Please include an \u0027@\u0027 in the email address. \u0027testadmin\u0027 is missing an \u0027@\u0027.\" at Email field",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.error_message_will_be_shown_at_Email_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG006 - User want to register with incomplete format email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG006"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email with inclomplete format \u0027testadmin@\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_email_with_inclomplete_format(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027xxxxxx\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Error message will be shown \"Please enter a part following \u0027@\u0027. \u0027testadmin@\u0027 is incomplete.\" at Email field",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.error_message_will_be_shown_at_Email_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG007 - User want to register using correct data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG007"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email \u0027testadmin123456@gmail.com\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027xxxxxxxx\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "popup meessage will be shown \u0027Silahkan verifikasi email agar dapat menggunakan layanan kami\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.popup_meessage_will_be_shown(String)"
});
formatter.result({
  "error_message": "com.kms.katalon.core.exception.StepFailedException: Call Test Case \u0027Test Cases/Pages/Page_Register/Read Popup Error Message\u0027 failed\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword$_callTestCase_closure1.doCall(CallTestCaseKeyword.groovy:63)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword$_callTestCase_closure1.call(CallTestCaseKeyword.groovy)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.runKeyword(KeywordMain.groovy:75)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword.callTestCase(CallTestCaseKeyword.groovy:81)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword.execute(CallTestCaseKeyword.groovy:44)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:74)\r\n\tat com.kms.katalon.core.keyword.BuiltinKeywords.callTestCase(BuiltinKeywords.groovy:334)\r\n\tat stepDefinition.Register.popup_meessage_will_be_shown(Register.groovy:77)\r\n\tat ✽.popup meessage will be shown \u0027Silahkan verifikasi email agar dapat menggunakan layanan kami\u0027(Include/features/Register.feature:80)\r\nCaused by: com.kms.katalon.core.exception.StepFailedException: Actual text \u0027Email sudah digunakan\u0027 and expected text \u0027Silahkan verifikasi email agar dapat menggunakan layanan kami\u0027 are not matched\r\n\tat com.kms.katalon.core.keyword.builtin.VerifyMatchKeyword$_verifyMatch_closure1.doCall(VerifyMatchKeyword.groovy:57)\r\n\tat com.kms.katalon.core.keyword.builtin.VerifyMatchKeyword$_verifyMatch_closure1.call(VerifyMatchKeyword.groovy)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.runKeyword(KeywordMain.groovy:75)\r\n\tat com.kms.katalon.core.keyword.builtin.VerifyMatchKeyword.verifyMatch(VerifyMatchKeyword.groovy:60)\r\n\tat com.kms.katalon.core.keyword.builtin.VerifyMatchKeyword.execute(VerifyMatchKeyword.groovy:45)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:74)\r\n\tat com.kms.katalon.core.keyword.BuiltinKeywords.verifyMatch(BuiltinKeywords.groovy:73)\r\n\tat Read Popup Error Message.run(Read Popup Error Message:27)\r\n\tat com.kms.katalon.core.main.ScriptEngine.run(ScriptEngine.java:194)\r\n\tat com.kms.katalon.core.main.ScriptEngine.runScriptAsRawText(ScriptEngine.java:119)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.runScript(TestCaseExecutor.java:448)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.doExecute(TestCaseExecutor.java:439)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.processExecutionPhase(TestCaseExecutor.java:418)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.accessMainPhase(TestCaseExecutor.java:410)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.execute(TestCaseExecutor.java:285)\r\n\tat com.kms.katalon.core.main.TestCaseMain.runTestCase(TestCaseMain.java:144)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword$_callTestCase_closure1.doCall(CallTestCaseKeyword.groovy:59)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword$_callTestCase_closure1.call(CallTestCaseKeyword.groovy)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.runKeyword(KeywordMain.groovy:75)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword.callTestCase(CallTestCaseKeyword.groovy:81)\r\n\tat com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword.execute(CallTestCaseKeyword.groovy:44)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:74)\r\n\tat com.kms.katalon.core.keyword.BuiltinKeywords.callTestCase(BuiltinKeywords.groovy:334)\r\n\tat stepDefinition.Register.popup_meessage_will_be_shown(Register.groovy:77)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:26)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:20)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:48)\r\n\tat cucumber.runtime.PickleStepDefinitionMatch.runStep(PickleStepDefinitionMatch.java:50)\r\n\tat cucumber.runner.TestStep.executeStep(TestStep.java:70)\r\n\tat cucumber.runner.TestStep.run(TestStep.java:52)\r\n\tat cucumber.runner.PickleStepTestStep.run(PickleStepTestStep.java:53)\r\n\tat cucumber.runner.TestCase.run(TestCase.java:47)\r\n\tat cucumber.runner.Runner.runPickle(Runner.java:44)\r\n\tat cucumber.runtime.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:140)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:68)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:23)\r\n\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)\r\n\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)\r\n\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:73)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:98)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:55)\r\n\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)\r\n\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)\r\n\tat cucumber.api.junit.Cucumber$1.evaluate(Cucumber.java:107)\r\n\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)\r\n\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)\r\n\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:105)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords$_runWithCucumberRunner_closure5.doCall(CucumberBuiltinKeywords.groovy:621)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords$_runWithCucumberRunner_closure5.doCall(CucumberBuiltinKeywords.groovy)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.runKeyword(KeywordMain.groovy:75)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.runKeyword(KeywordMain.groovy:69)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain$runKeyword.call(Unknown Source)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords.runWithCucumberRunner(CucumberBuiltinKeywords.groovy:618)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords$runWithCucumberRunner$0.callStatic(Unknown Source)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords.runWithCucumberRunner(CucumberBuiltinKeywords.groovy:718)\r\n\tat com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords$runWithCucumberRunner.call(Unknown Source)\r\n\tat Runner.run(Runner:20)\r\n\tat com.kms.katalon.core.main.ScriptEngine.run(ScriptEngine.java:194)\r\n\tat com.kms.katalon.core.main.ScriptEngine.runScriptAsRawText(ScriptEngine.java:119)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.runScript(TestCaseExecutor.java:448)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.doExecute(TestCaseExecutor.java:439)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.processExecutionPhase(TestCaseExecutor.java:418)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.accessMainPhase(TestCaseExecutor.java:410)\r\n\tat com.kms.katalon.core.main.TestCaseExecutor.execute(TestCaseExecutor.java:285)\r\n\tat com.kms.katalon.core.main.TestCaseMain.runTestCase(TestCaseMain.java:144)\r\n\tat com.kms.katalon.core.main.TestCaseMain.runTestCase(TestCaseMain.java:135)\r\n\tat com.kms.katalon.core.main.TestCaseMain$runTestCase$0.call(Unknown Source)\r\n\tat TempTestCase1685780924385.run(TempTestCase1685780924385.groovy:25)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "REG008 - User want to register using registered email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG008"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email \u0027raayhanmuhammad.kerjaan@gmail.com\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027rayhan691971\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "popup message will be shown \u0027Email sudah digunakan\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.popup_message_will_be_shown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG009 - User want to register using unverified email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG009"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input nama \u0027rayhan muhammad\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input email \u0027rayhanmuhammad@gmail.com\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input password \u0027rayhan691971\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Daftar button at Register page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_Daftar_button_at_Register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "popup message will be shown \u0027Email sudah digunakan\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.popup_message_will_be_shown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG010 - User want to access login page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG010"
    }
  ]
});
formatter.step({
  "name": "User open browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_open_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Masuk button at Homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_click_on_Masuk_button_at_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Daftar at Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Daftar_at_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on link Masuk disini",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_click_on_link_Masuk_disini()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_close_browser()"
});
formatter.result({
  "status": "passed"
});
});