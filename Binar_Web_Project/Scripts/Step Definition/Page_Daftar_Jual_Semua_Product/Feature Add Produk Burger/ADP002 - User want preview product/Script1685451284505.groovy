import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//login secondhand
WebUI.callTestCase(findTestCase('Step Definition/Page_Login/Feature Login/LGI006 - User want to login using correct credential'),
	[('email_list') : 'maghfira334@gmail.com', ('password_list') : '123456789'], FailureHandling.STOP_ON_FAILURE)

//click burger menu
WebUI.callTestCase(findTestCase('Test Cases/Pages/Header/Click Burger Menu'), [:], FailureHandling.STOP_ON_FAILURE)

//click tambah produk
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Click Tambah Produk'), [:], FailureHandling.STOP_ON_FAILURE)

//input nama
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Input Nama Produk'), [('nama') : 'baju anak'], FailureHandling.STOP_ON_FAILURE)

//input harga
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Input Harga Produk'), [('harga') : '15000'], FailureHandling.STOP_ON_FAILURE)

//pilih kategori
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Pilih Kategori'), [:], FailureHandling.STOP_ON_FAILURE)

//input deskripsi
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Input Deskripsi'), [('desc') : 'baju bagus Asli'], FailureHandling.STOP_ON_FAILURE)

//fitur add produk error dari web nya
//WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Click Add Foto'), [:], FailureHandling.STOP_ON_FAILURE)

//klik preview (error preview dari webnya)
WebUI.callTestCase(findTestCase('Test Cases/Pages/Page_Add_Product_Burger/Click Preview'), [:], FailureHandling.STOP_ON_FAILURE)