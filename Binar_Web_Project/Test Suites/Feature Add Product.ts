<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Feature Add Product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b9ee8eba-e888-4694-9773-22ced6717e93</testSuiteGuid>
   <testCaseLink>
      <guid>bcdb39ec-0e43-4cdf-9465-163f96591cc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Daftar_Jual_Semua_Product/Feature Add Produk Burger/ADP001- User wants to add a product on burger menu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>22bf3c2d-f98c-4f6b-9ce1-81fc3f29e026</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Daftar_Jual_Semua_Product/Feature Add Produk Burger/ADP002 - User want preview product</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3b67efa-cc61-434a-b8a3-ae0fe0571510</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Home/Feature Add Product Home/ADP003 - User wants to add a product using different email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d84b5523-fa68-4541-989c-851455cf0fda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Home/Feature Add Product Home/ADP004 - User wants to add a product without login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
