<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_product_image</name>
   <tag></tag>
   <elementGuidId>0b7b075e-26b3-454f-a283-0396555a49ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div[2]/div/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>img.card-img-top</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>98c95292-21a8-492d-aab7-b1185fa77471</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>http://res.cloudinary.com/dgaeremr2/image/upload/v1666597259/Products-Customers/isn3pogu1r5av2v5824r.jpg</value>
      <webElementGuid>7f69c741-e319-4a3c-b84e-5afdeef03089</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-img-top</value>
      <webElementGuid>380e48ea-ad19-49ba-ae38-ca26d7aae73d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Product Sarung</value>
      <webElementGuid>fe2febe3-f092-49b1-9e2d-78702d0220da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;products&quot;]/div[@class=&quot;card&quot;]/img[@class=&quot;card-img-top&quot;]</value>
      <webElementGuid>f4e215c2-97db-49c0-8c41-2d1aebfca64f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div[2]/div/img</value>
      <webElementGuid>796d3c60-3b98-4612-bdbd-3d244d20e632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jual'])[1]/preceding::img[147]</value>
      <webElementGuid>90f849a4-a858-4238-89e1-616d2e871f46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::img[147]</value>
      <webElementGuid>fbba7130-a652-4a9b-ba23-8ffbbf7f8e80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Product Sarung']</value>
      <webElementGuid>e7cfb57b-a2a8-47e1-a6ef-f48a8253969b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/img</value>
      <webElementGuid>40222ee4-c7f2-40fb-b1a6-bb779887204c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'http://res.cloudinary.com/dgaeremr2/image/upload/v1666597259/Products-Customers/isn3pogu1r5av2v5824r.jpg' and @alt = 'Product Sarung']</value>
      <webElementGuid>0a903c25-b7ee-4e61-96db-dfb83746c59b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
