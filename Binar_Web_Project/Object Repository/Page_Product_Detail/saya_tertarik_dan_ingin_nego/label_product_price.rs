<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_product_price</name>
   <tag></tag>
   <elementGuidId>2f9736b3-9eea-4021-ab95-741eec2b08a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.product > div.card-body > div > p.card-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Gitar'])[2]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b201615b-d108-4c1d-a412-fba0471d1c4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-text</value>
      <webElementGuid>8a2e8526-d1c8-4d77-b6e8-89eadd14841d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Rp 2.500.000,00</value>
      <webElementGuid>498f5e6e-f7ea-4389-871f-c628763eec7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/div[1]/p[@class=&quot;card-text&quot;]</value>
      <webElementGuid>fc8d1676-56a9-4b53-b1f5-b98defa9ac62</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gitar'])[2]/following::p[1]</value>
      <webElementGuid>a12d29cc-0268-42e1-bcd0-fc5e8f8dbaae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::p[2]</value>
      <webElementGuid>80f216f1-ff75-4248-8c07-f58364481440</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Tawar'])[1]/preceding::p[1]</value>
      <webElementGuid>0600dc8b-07d6-43e0-90f8-4faefe3796fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim'])[1]/preceding::p[1]</value>
      <webElementGuid>acd59875-42f0-4ae8-b346-2c3e86d1c8a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/p</value>
      <webElementGuid>8fd8712d-f20b-44ef-af13-80aa7afed50a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = ' Rp 2.500.000,00' or . = ' Rp 2.500.000,00')]</value>
      <webElementGuid>d945ae54-7170-44c0-bd27-49ccb34f96cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
